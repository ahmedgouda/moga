<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Mooga | Add new offer</title>
    <meta charset="utf-8" dir="rtl">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<header>
    <div class="top-header">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="social-media header-social">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                    <li><a href="#"><i class="fa fa-rss"></i></a></li>
                </ul>
                <p class="text-left white-font md-font half-padding table-left">
                    <a href="" class="md-font white-link"> إتصل بنا</a>
                    -
                    <a href="" class="md-font white-link"> من نحن</a>
                </p>
                <p class="text-right white-font md-font half-padding table-right">
                    <a href="" class="md-font white-link text-right right"><i class="lock-cust"></i>تسجيل الدخول</a>
                </p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 round-corner no-overflow">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand main-logo" href="#"></a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav alg-left">
                            <li class="active"><a href="#">الرئيسية <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">مشروعات  </a></li>
                            <li><a href="#"> تصفيات </a></li>
                            <li><a href="#">عروض وفرص </a></li>
                            <li><a href="#">لمقالات</a></li>
                            <li class="search-link"><a href="#"><i class="fa fa-search"></i></a></li>

                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div><!--new navigation-->
<!---------------------------------------------------------------------------------------------------------------------------------->
    <!--<div class="top-header">
        <ul class="social-media header-social">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
            <li><a href="#"><i class="fa fa-rss"></i></a></li>
        </ul>
        <p class="text-left white-font md-font half-padding table-left">
            <a href="" class="md-font white-link"> إتصل بنا</a>
            -
            <a href="" class="md-font white-link"> من نحن</a>
        </p>
        <p class="text-right white-font md-font half-padding table-right">
            <a href="" class="md-font white-link text-right right"><i class="lock-cust"></i>تسجيل الدخول</a>
        </p>
    </div>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand main-logo" href="#"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav alg-left">
                    <li class="active"><a href="#">الرئيسية <span class="sr-only">(current)</span></a></li>
                    <li><a href="#">مشروعات  </a></li>
                    <li><a href="#"> تصفيات </a></li>
                    <li><a href="#">عروض وفرص </a></li>
                    <li><a href="#">لمقالات</a></li>
                    <li class="search-link"><a href="#"><i class="fa fa-search"></i></a></li>

                </ul>
            </div>
        </div>
    </nav>-old navigation -->
<!---------------------------------------------------------------------------------------------------------------------------------->
</header>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border round-corner internal-sub-header btm-mrg-sm grey">
            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 def-padding-sm">
                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                    <img src="images/logged-user.jpg" class="round-corner justified-img">
                </div>
                <div class="col-md-8 col-lg-8 col-xs-12 col-sm-12 ">
                    <p class="orange-font md-lg-font text-right">طارق عبدالغني المليجي</p>
                    <p class="grey-font md-font text-right">مبرمج جافا أول ومحرر تقني</p>
                </div>
            </div>
            <div class="col-md-8 col-lg-8 col-xs-12 col-sm-12 def-padding-sm justified-div">
                <a href="#" class="header-btn md-font grey-font"><i class="btn-ico cash"></i>الرصيد  </a>
                <a href="#" class="header-btn md-font grey-font"><i class="btn-ico light"><span class="notification green">2</span></i> الفرص</a>
                <a href="#" class="header-btn md-font grey-font"><i class="btn-ico case"><span class="notification green">7</span></i> المشروعات</a>
                <a href="#" class="header-btn md-font grey-font"><i class="btn-ico message"><span class="notification red">20</span></i>  الرسائل</a>
                <a href="#" class="header-btn md-font grey-font"><i class="btn-ico settings"></i>  إعدادات الحساب</a>
            </div>
        </div>
        <div class="col-lg-12 col-xs-12 col-md-12 col-xs-12 def-padding">
            <div class="col-md-10 col-lg-10 col-xs-12 col-sm-12 center-table">
                <h2 class="full-lines sm-font"><span class="blue-font">اضف عرض جديد</span></h2>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border def-padding">
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 ">
                            <div class="col-md-10 col-lg-10 col-xs-10 col-sm-10 center-table">
                                <form class="login-form" action="{{url("/")}}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label md-font bold black-font">استشاري في مجال</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" id="">
                                                <option>اختر</option>
                                                <option value="1"> البناء و التطوير</option>
                                                <option value="2">الطب </option>
                                                <option value="3"> البرمجة</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label md-font bold black-font">اختر منتج او خدمه</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="name" class="form-control" id="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label md-font bold black-font">البلد</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="country" class="form-control" id="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label md-font bold black-font">احتاج وكيل في</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="agent" class="form-control" id="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label md-font bold black-font">احتاج مستثمر</label>
                                        <div class="col-sm-4">
                                            <input type="text"  class="form-control" id="" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label md-font bold black-font">الهدف من العرض</label>
                                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                                <label class="radio-inline">
                                                    <input type="radio" name="lead" id="inlineRadio1" value="option1">  رائد أعمال
                                                </label>
                                            </div>
                                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                                <label class="radio-inline">
                                                    <input type="radio" name="manager" id="inlineRadio1" value="option1">  مدير
                                                </label>
                                            </div>
                                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                                <label class="radio-inline">
                                                    <input type="radio" name="adviser" id="inlineRadio1" value="option1">مستشار
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="col-sm-3 control-label md-font bold black-font">توصيف العرض</label>
                                        <div class="col-sm-9">
                                            <textarea name="desc" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn orange-btn">اضف</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="container">
    <div class="row footer-dark round-corner">
        <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 def-padding">
            <p class="md-font white-font">
                2016 -  كامل الحقوق محفوظة  لشركةموجة
                <br>
                <a href="" class="md-font white-link">سياسة الخصوصية</a>
                -
                <a href="" class="md-font white-link"> بنود الإستخدام</a>
            </p>
            <ul class="social-media">
                <li><a href="#"><i class="fa fa-facebook round-corner"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter round-corner"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin round-corner"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube round-corner"></i></a></li>
                <li><a href="#"><i class="fa fa-rss round-corner"></i></a></li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 def-padding">
            <ul class="footer-nav">
                <li><a class="md-font white-link" href=""> ريادة أعـمال  </a></li>
                <li><a class="md-font white-link" href=""> تســويق     </a></li>
                <li><a class="md-font white-link" href=""> مبيــعات       </a></li>
                <li><a class="md-font white-link" href=""> إدارة أعــمال  </a></li>
                <li><a class="md-font white-link" href=""> قـصص النجـاح</a></li>
            </ul>
            <ul class="footer-nav">
                <li><a class="md-font white-link" href=""> الصفحة الرئيسة</a></li>
                <li><a class="md-font white-link" href="">من نحن</a></li>
                <li><a class="md-font white-link" href="">إتصل بنا</a></li>
            </ul>
        </div>
    </div>
</div>
</body>
</html>

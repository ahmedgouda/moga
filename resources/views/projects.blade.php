<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Mooga | All projects</title>
    <meta charset="utf-8" dir="rtl">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<header>
    <div class="top-header">
        <ul class="social-media header-social">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
            <li><a href="#"><i class="fa fa-rss"></i></a></li>
        </ul>
        <p class="text-left white-font md-font half-padding table-left">
            <a href="" class="md-font white-link"> إتصل بنا</a>
            -
            <a href="" class="md-font white-link"> من نحن</a>
        </p>
        <p class="text-right white-font md-font half-padding table-right">
            <a href="" class="md-font white-link text-right right"><i class="lock-cust"></i>تسجيل الدخول</a>
        </p>
    </div>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand main-logo" href="#"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav alg-left">
                    <li class="active"><a href="#">الرئيسية <span class="sr-only">(current)</span></a></li>
                    <li><a href="#">مشروعات  </a></li>
                    <li><a href="#"> تصفيات </a></li>
                    <li><a href="#">عروض وفرص </a></li>
                    <li><a href="#">لمقالات</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>


<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border round-corner internal-sub-header btm-mrg-sm">
            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 def-padding-sm">
                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                    <img src="images/logged-user.jpg" class="round-corner justified-img">
                </div>
                <div class="col-md-8 col-lg-8 col-xs-12 col-sm-12 ">
                    <p class="orange-font md-lg-font text-right">طارق عبدالغني المليجي</p>
                    <p class="grey-font md-font text-right">مبرمج جافا أول ومحرر تقني</p>
                </div>
            </div>
            <div class="col-md-8 col-lg-8 col-xs-12 col-sm-12 def-padding-sm justified-div">
                <a href="#" class="header-btn md-font grey-font"><i class="btn-ico cash"></i>الرصيد  </a>
                <a href="#" class="header-btn md-font grey-font"><i class="btn-ico light"><span class="notification green">2</span></i> الفرص</a>
                <a href="#" class="header-btn md-font grey-font"><i class="btn-ico case"><span class="notification green">7</span></i> المشروعات</a>
                <a href="#" class="header-btn md-font grey-font"><i class="btn-ico message"><span class="notification red">20</span></i>  الرسائل</a>
                <a href="#" class="header-btn md-font grey-font"><i class="btn-ico settings"></i>  إعدادات الحساب</a>
            </div>
        </div>
		
		
		
        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 triangle-tabs no-border">
            <ul class="nav nav-tabs nav-justified zero-right-padding " role="tablist">
                <li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">ابحث في مجتمع موجة</a></li>
                <li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab"  data-toggle="tab">ابحث في المشروعات</a></li>
                <li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab"  data-toggle="tab">ابحث في التصفيات</a></li>
                <li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab"  data-toggle="tab">ابحث في الفرص والعروض</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane " id="tab1">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border round-corner def-padding-sm grey btm-mrg-sm">
                        <div class="col-md-5 col-lg-5 col-xs-12 col-sm-12 def-padding left-border">
                            <h2 class="line-right sm-font zero-bottom-margin"><span class="black-font">                                أبحث عن خبرات</span></h2>
                            <form class="cust-form">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <select class="form-control">
                                            <option>استشاري في مجال</option>
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="" placeholder="أحتاج منتج أوخدمة">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">أحتاج وكيل في</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">أحتاج مستثمر </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                                <button type="submit" class="btn orange-btn">إبحث</button>
                            </form>
                        </div>
                        <div class="col-md-5 col-lg-5 col-xs-12 col-sm-12 def-padding left-border">
                            <h2 class="line-right sm-font zero-bottom-margin"><span class="black-font">                                أبحث عن خبرات</span></h2>

                            <form class="cust-form">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <select class="form-control">
                                            <option>استشاري في مجال</option>
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="" placeholder="أحتاج منتج أوخدمة">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">أحتاج وكيل في</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-4 control-label">أحتاج مستثمر </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                                <button type="submit" class="btn orange-btn">إبحث</button>
                            </form>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12 def-padding">
                            <p class="black-font sm-font text-center">
                                إقترح لي
                            </p>
                            <p class="grey-font md-font text-justify">
                                الاسم (وجنبه عدد المشروعات اللي تحت المستخدم ده)، والصورة ، البلد   ويتكتب استشاري في مجالات كذا
                            </p>
                            <button type="submit" class="btn orange-btn">   إبدأ</button>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border round-corner def-padding-sm lg-btm-mrg">
                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 left-border def-padding-sm" >
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <img src="images/project-owner.jpg" class="justified-img">
                                <p class="black-font sm-font text-center bold">
                                    محمود  خليفة
                                </p>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <p class="sm-font text-right orange-font zero-bottom-margin">
                                    رائد أعمال في التسويق
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    البلد : مصر
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    المشروعات :  7
                                </p>
                                <a class="sm-border-btn round-corner grey-border"><i class="message-box"></i></a>
                                <a class="sm-border-btn round-corner grey-border"><i class="call-icon"></i></a>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-xs-12 col-sm-12 def-padding-sm">
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    خبرات في: الإدارة، التسويق، خدمة العملاء....
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أقدم منتج/خدمة: مواقع معلوماتية
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أعمل وكيل لـ: برنامج مراقبة المواقع  ...
                                </p>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    ابحث عن استشاري في: الاستضافة، نظم سير العمل ...
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أحتاج منتج/خدمة: إدخال بيانات، برامج محاسبة.
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أحتاج وكيل: لا يوجد
                                </p>
                            </div>
                        </div>
                        <div class="left-btns-cont-bottom"><a href="#" class="btn white-btn md-font text-center border-btn">المزيد ...</a></div>
                    </div><!--end single item-->
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border round-corner def-padding-sm lg-btm-mrg">
                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 left-border def-padding-sm" >
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <img src="images/project-owner.jpg" class="justified-img">
                                <p class="black-font sm-font text-center bold">
                                    محمود  خليفة
                                </p>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <p class="sm-font text-right orange-font zero-bottom-margin">
                                    رائد أعمال في التسويق
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    البلد : مصر
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    المشروعات :  7
                                </p>
                                <a class="sm-border-btn round-corner grey-border"><i class="message-box"></i></a>
                                <a class="sm-border-btn round-corner grey-border"><i class="call-icon"></i></a>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-xs-12 col-sm-12 def-padding-sm">
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    خبرات في: الإدارة، التسويق، خدمة العملاء....
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أقدم منتج/خدمة: مواقع معلوماتية
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أعمل وكيل لـ: برنامج مراقبة المواقع  ...
                                </p>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    ابحث عن استشاري في: الاستضافة، نظم سير العمل ...
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أحتاج منتج/خدمة: إدخال بيانات، برامج محاسبة.
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أحتاج وكيل: لا يوجد
                                </p>
                            </div>
                        </div>
                        <div class="left-btns-cont-bottom"><a href="#" class="btn white-btn md-font text-center border-btn">المزيد ...</a></div>
                    </div><!--end single item-->
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border round-corner def-padding-sm lg-btm-mrg">
                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 left-border def-padding-sm" >
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <img src="images/project-owner.jpg" class="justified-img">
                                <p class="black-font sm-font text-center bold">
                                    محمود  خليفة
                                </p>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <p class="sm-font text-right orange-font zero-bottom-margin">
                                    رائد أعمال في التسويق
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    البلد : مصر
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    المشروعات :  7
                                </p>
                                <a class="sm-border-btn round-corner grey-border"><i class="message-box"></i></a>
                                <a class="sm-border-btn round-corner grey-border"><i class="call-icon"></i></a>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-xs-12 col-sm-12 def-padding-sm">
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    خبرات في: الإدارة، التسويق، خدمة العملاء....
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أقدم منتج/خدمة: مواقع معلوماتية
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أعمل وكيل لـ: برنامج مراقبة المواقع  ...
                                </p>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    ابحث عن استشاري في: الاستضافة، نظم سير العمل ...
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أحتاج منتج/خدمة: إدخال بيانات، برامج محاسبة.
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أحتاج وكيل: لا يوجد
                                </p>
                            </div>
                        </div>
                        <div class="left-btns-cont-bottom"><a href="#" class="btn white-btn md-font text-center border-btn">المزيد ...</a></div>
                    </div><!--end single item-->
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border round-corner def-padding-sm lg-btm-mrg">
                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 left-border def-padding-sm" >
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <img src="images/project-owner.jpg" class="justified-img">
                                <p class="black-font sm-font text-center bold">
                                    محمود  خليفة
                                </p>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <p class="sm-font text-right orange-font zero-bottom-margin">
                                    رائد أعمال في التسويق
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    البلد : مصر
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    المشروعات :  7
                                </p>
                                <a class="sm-border-btn round-corner grey-border"><i class="message-box"></i></a>
                                <a class="sm-border-btn round-corner grey-border"><i class="call-icon"></i></a>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-xs-12 col-sm-12 def-padding-sm">
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    خبرات في: الإدارة، التسويق، خدمة العملاء....
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أقدم منتج/خدمة: مواقع معلوماتية
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أعمل وكيل لـ: برنامج مراقبة المواقع  ...
                                </p>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    ابحث عن استشاري في: الاستضافة، نظم سير العمل ...
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أحتاج منتج/خدمة: إدخال بيانات، برامج محاسبة.
                                </p>
                                <p class="md-font text-right grey-font zero-bottom-margin">
                                    أحتاج وكيل: لا يوجد
                                </p>
                            </div>
                        </div>
                        <div class="left-btns-cont-bottom"><a href="#" class="btn white-btn md-font text-center border-btn">المزيد ...</a></div>
                    </div><!--end single item-->
                </div>
                <div role="tabpanel" class="tab-pane active" id="tab2">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border round-corner def-padding-sm grey btm-mrg-sm">
                        <div class="col-md-9 col-lg-9 col-xs-12 col-sm-12 def-padding left-border ">
                            <h2 class="line-right sm-font zero-bottom-margin"><span class="black-font">تصنيفات البحث</span></h2>
                            <form class="cust-form col-md-10 col-lg-offset-2 col-md-10 col-lg-offset-2 ">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">ابحث عن </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">في دولة</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">المرحلة</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9">
                                        <label for="" class="col-sm-3 control-label">المشروع يحتاج </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="inlineCheckbox1" value="option1">موزع
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="inlineCheckbox2" value="option2">شريك
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="inlineCheckbox3" value="option3">ممول
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <a href="" class="btn white-btn alg-left mrg-thirty">المزيد</a>
                                    </div>
                                </div>
                                <div class="col-sm-8 col-sm-offset-4">
                                    <button type="submit" class="btn orange-btn">إبحث</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12 def-padding">
                            <p class="black-font sm-font text-center">
                                إقترح لي
                            </p>
                            <p class="grey-font md-font text-justify">
                                الاسم (وجنبه عدد المشروعات اللي تحت المستخدم ده)، والصورة ، البلد   ويتكتب استشاري في مجالات كذا
                            </p>
                            <button type="submit" class="btn orange-btn">   إبدأ</button>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border round-corner def-padding-sm lg-btm-mrg">
                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 left-border def-padding-sm" >
                            <p class="black-font sm-font text-right bold">
                                إعادة تدوير مخلفات
                            </p>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <img src="images/project-owner.jpg" class="justified-img">
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">المشروع:  </p>
                                    <p class="md-font grey-font zero-bottom-margin">تحت التطوير</p>
                                </div>
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">الدولة: </p>
                                    <p class="md-font  grey-font zero-bottom-margin ">جمهورية مصر العربية</p>
                                </div>
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">المشاهدات:</p>
                                    <p class="md-font  grey-font zero-bottom-margin ">12</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12 left-border def-padding-sm">
                            <p class="md-font text-right grey-font zero-bottom-margin sm-top-padding">
                                كثير من أكبر وأنجح رجال الأعمال في العالم بدأوا وليس لديهم غير حب المغامره وشغفهم بفكرتهم ومشروعهم وثقتهم في نفسهم...الموضوع مش صعب ولا مستحيل كثير من أكبر وأنجح رجال الأعمال في العالم بدأوا وليس لديهم غير حب المغامره وشغفهم بفكرتهم ومشروعهم وثقتهم في نفسهم...الموضوع مش صعب ولا مستحيل
                            </p>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12  def-padding-sm" >
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <p class="black-font sm-font text-center bold">
                                    مالك المشروع
                                </p>
                                <img src="images/project-owner.jpg" class="justified-img">
                                <p class="black-font sm-font text-center bold zero-bottom-margin">
                                    محمود  خليفة
                                </p>
                                <div class="abs-btn cst">
                                    <a class="sm-border-btn round-corner grey-border"><i class="message-box"></i></a>
                                    <a class="sm-border-btn round-corner grey-border"><i class="call-icon"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="left-btns-cont-bottom center-button"><a href="#" class="btn white-btn md-font text-center border-btn">المزيد ...</a></div>

                    </div><!--end single item-->
                </div>
                <div role="tabpanel" class="tab-pane  " id="tab3"></div>
                <div role="tabpanel" class="tab-pane  " id="tab4">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border round-corner def-padding-sm grey btm-mrg-sm">
                        <div class="col-md-9 col-lg-9 col-xs-12 col-sm-12 def-padding left-border">
                            <h2 class="line-right sm-font zero-bottom-margin"><span class="black-font">تصنيفات البحث</span></h2>
                            <form class="cust-form col-md-10 col-lg-offset-2 col-md-10 col-lg-offset-2 ">
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">ابحث عن </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">في دولة</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">المرحلة</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="col-sm-8 col-sm-offset-4">
                                    <button type="submit" class="btn orange-btn">إبحث</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12 def-padding">
                            <p class="black-font sm-font text-center">
                                إقترح لي
                            </p>
                            <p class="grey-font md-font text-justify">
                                الاسم (وجنبه عدد المشروعات اللي تحت المستخدم ده)، والصورة ، البلد   ويتكتب استشاري في مجالات كذا
                            </p>
                            <button type="submit" class="btn orange-btn">   إبدأ</button>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border round-corner def-padding-sm lg-btm-mrg">
                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 left-border def-padding-sm" >
                            <p class="black-font sm-font text-right bold">
                                إعادة تدوير مخلفات
                            </p>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <img src="images/project-owner.jpg" class="justified-img">
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">المشروع:  </p>
                                    <p class="md-font grey-font zero-bottom-margin">تحت التطوير</p>
                                </div>
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">الدولة: </p>
                                    <p class="md-font  grey-font zero-bottom-margin ">جمهورية مصر العربية</p>
                                </div>
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">المشاهدات:</p>
                                    <p class="md-font  grey-font zero-bottom-margin ">12</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12 left-border def-padding-sm">
                            <p class="md-font text-right grey-font zero-bottom-margin sm-top-padding">
                                كثير من أكبر وأنجح رجال الأعمال في العالم بدأوا وليس لديهم غير حب المغامره وشغفهم بفكرتهم ومشروعهم وثقتهم في نفسهم...الموضوع مش صعب ولا مستحيل كثير من أكبر وأنجح رجال الأعمال في العالم بدأوا وليس لديهم غير حب المغامره وشغفهم بفكرتهم ومشروعهم وثقتهم في نفسهم...الموضوع مش صعب ولا مستحيل
                            </p>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12  def-padding-sm" >
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <p class="black-font sm-font text-center bold">
                                    مالك المشروع
                                </p>
                                <img src="images/project-owner.jpg" class="justified-img">
                                <p class="black-font sm-font text-center bold zero-bottom-margin">
                                    محمود  خليفة
                                </p>
                                <div class="abs-btn cst">
                                    <a class="sm-border-btn round-corner grey-border"><i class="message-box"></i></a>
                                    <a class="sm-border-btn round-corner grey-border"><i class="call-icon"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="left-btns-cont-bottom center-button"><a href="#" class="btn white-btn md-font text-center border-btn">المزيد ...</a></div>

                    </div><!--end single item-->
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border round-corner def-padding-sm lg-btm-mrg">
                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 left-border def-padding-sm" >
                            <p class="black-font sm-font text-right bold">
                                إعادة تدوير مخلفات
                            </p>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <img src="images/project-owner.jpg" class="justified-img">
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">المشروع:  </p>
                                    <p class="md-font grey-font zero-bottom-margin">تحت التطوير</p>
                                </div>
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">الدولة: </p>
                                    <p class="md-font  grey-font zero-bottom-margin ">جمهورية مصر العربية</p>
                                </div>
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">المشاهدات:</p>
                                    <p class="md-font  grey-font zero-bottom-margin ">12</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12 left-border def-padding-sm">
                            <p class="md-font text-right grey-font zero-bottom-margin sm-top-padding">
                                كثير من أكبر وأنجح رجال الأعمال في العالم بدأوا وليس لديهم غير حب المغامره وشغفهم بفكرتهم ومشروعهم وثقتهم في نفسهم...الموضوع مش صعب ولا مستحيل كثير من أكبر وأنجح رجال الأعمال في العالم بدأوا وليس لديهم غير حب المغامره وشغفهم بفكرتهم ومشروعهم وثقتهم في نفسهم...الموضوع مش صعب ولا مستحيل
                            </p>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12  def-padding-sm" >
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <p class="black-font sm-font text-center bold">
                                    مالك المشروع
                                </p>
                                <img src="images/project-owner.jpg" class="justified-img">
                                <p class="black-font sm-font text-center bold zero-bottom-margin">
                                    محمود  خليفة
                                </p>
                                <div class="abs-btn cst">
                                    <a class="sm-border-btn round-corner grey-border"><i class="message-box"></i></a>
                                    <a class="sm-border-btn round-corner grey-border"><i class="call-icon"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="left-btns-cont-bottom center-button"><a href="#" class="btn white-btn md-font text-center border-btn">المزيد ...</a></div>

                    </div><!--end single item-->
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border round-corner def-padding-sm lg-btm-mrg">
                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 left-border def-padding-sm" >
                            <p class="black-font sm-font text-right bold">
                                إعادة تدوير مخلفات
                            </p>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <img src="images/project-owner.jpg" class="justified-img">
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">المشروع:  </p>
                                    <p class="md-font grey-font zero-bottom-margin">تحت التطوير</p>
                                </div>
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">الدولة: </p>
                                    <p class="md-font  grey-font zero-bottom-margin ">جمهورية مصر العربية</p>
                                </div>
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">المشاهدات:</p>
                                    <p class="md-font  grey-font zero-bottom-margin ">12</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12 left-border def-padding-sm">
                            <p class="md-font text-right grey-font zero-bottom-margin sm-top-padding">
                                كثير من أكبر وأنجح رجال الأعمال في العالم بدأوا وليس لديهم غير حب المغامره وشغفهم بفكرتهم ومشروعهم وثقتهم في نفسهم...الموضوع مش صعب ولا مستحيل كثير من أكبر وأنجح رجال الأعمال في العالم بدأوا وليس لديهم غير حب المغامره وشغفهم بفكرتهم ومشروعهم وثقتهم في نفسهم...الموضوع مش صعب ولا مستحيل
                            </p>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12  def-padding-sm" >
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <p class="black-font sm-font text-center bold">
                                    مالك المشروع
                                </p>
                                <img src="images/project-owner.jpg" class="justified-img">
                                <p class="black-font sm-font text-center bold zero-bottom-margin">
                                    محمود  خليفة
                                </p>
                                <div class="abs-btn cst">
                                    <a class="sm-border-btn round-corner grey-border"><i class="message-box"></i></a>
                                    <a class="sm-border-btn round-corner grey-border"><i class="call-icon"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="left-btns-cont-bottom center-button"><a href="#" class="btn white-btn md-font text-center border-btn">المزيد ...</a></div>

                    </div><!--end single item-->
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12 grey-border round-corner def-padding-sm lg-btm-mrg">
                        <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 left-border def-padding-sm" >
                            <p class="black-font sm-font text-right bold">
                                إعادة تدوير مخلفات
                            </p>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <img src="images/project-owner.jpg" class="justified-img">
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">المشروع:  </p>
                                    <p class="md-font grey-font zero-bottom-margin">تحت التطوير</p>
                                </div>
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">الدولة: </p>
                                    <p class="md-font  grey-font zero-bottom-margin ">جمهورية مصر العربية</p>
                                </div>
                                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                    <p class="md-font  orange-font zero-bottom-margin cst-right-alignment">المشاهدات:</p>
                                    <p class="md-font  grey-font zero-bottom-margin ">12</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12 left-border def-padding-sm">
                            <p class="md-font text-right grey-font zero-bottom-margin sm-top-padding">
                                كثير من أكبر وأنجح رجال الأعمال في العالم بدأوا وليس لديهم غير حب المغامره وشغفهم بفكرتهم ومشروعهم وثقتهم في نفسهم...الموضوع مش صعب ولا مستحيل كثير من أكبر وأنجح رجال الأعمال في العالم بدأوا وليس لديهم غير حب المغامره وشغفهم بفكرتهم ومشروعهم وثقتهم في نفسهم...الموضوع مش صعب ولا مستحيل
                            </p>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xs-12 col-sm-12  def-padding-sm" >
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <p class="black-font sm-font text-center bold">
                                    مالك المشروع
                                </p>
                                <img src="images/project-owner.jpg" class="justified-img">
                                <p class="black-font sm-font text-center bold zero-bottom-margin">
                                    محمود  خليفة
                                </p>
                                <div class="abs-btn cst">
                                    <a class="sm-border-btn round-corner grey-border"><i class="message-box"></i></a>
                                    <a class="sm-border-btn round-corner grey-border"><i class="call-icon"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="left-btns-cont-bottom center-button"><a href="#" class="btn white-btn md-font text-center border-btn">المزيد ...</a></div>

                    </div><!--end single item-->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row footer-dark round-corner">
        <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 def-padding">
            <p class="md-font white-font">
                2016 -  كامل الحقوق محفوظة  لشركةموجة
                <br>
                <a href="" class="md-font white-link">سياسة الخصوصية</a>
                -
                <a href="" class="md-font white-link"> بنود الإستخدام</a>
            </p>
            <ul class="social-media">
                <li><a href="#"><i class="fa fa-facebook round-corner"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter round-corner"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin round-corner"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube round-corner"></i></a></li>
                <li><a href="#"><i class="fa fa-rss round-corner"></i></a></li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 def-padding">
            <ul class="footer-nav">
                <li><a class="md-font white-link" href=""> ريادة أعـمال  </a></li>
                <li><a class="md-font white-link" href=""> تســويق     </a></li>
                <li><a class="md-font white-link" href=""> مبيــعات       </a></li>
                <li><a class="md-font white-link" href=""> إدارة أعــمال  </a></li>
                <li><a class="md-font white-link" href=""> قـصص النجـاح</a></li>
            </ul>
            <ul class="footer-nav">
                <li><a class="md-font white-link" href=""> الصفحة الرئيسة</a></li>
                <li><a class="md-font white-link" href="">من نحن</a></li>
                <li><a class="md-font white-link" href="">إتصل بنا</a></li>
            </ul>
        </div>
    </div>
</div>

</body>
</html>
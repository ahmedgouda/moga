<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\projects;
use Illuminate\Support\Facades\Input;
use phpDocumentor\Reflection\Project;

class projectsController extends Controller
{
    public function index(){

        $projects = projects::get();
        return view('projects',compact('projects'));

    }
    public function create(){

        return view('add-new-offer');
    }
    public function store(Request $request){

        $inputs = Input::all();
        return $inputs;

    }
    public function edit($id){

        $project =projects::find($id);
        return view('single-project',compact('project'));
    }
    public function show($id){
        $project =projects::find($id);
        return view('single-project',compact('project'));

    }
    public function update($id){


    }
    public function delete(){

    }
}
